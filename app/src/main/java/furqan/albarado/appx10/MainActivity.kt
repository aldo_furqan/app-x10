package furqan.albarado.appx10

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnLogin.setOnClickListener(this)
        txSignUp.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.btnLogin ->{
                var email = edUserName.text.toString()
                var password = edPassword.text.toString()

                if (email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(this, "Username / password can't be empty", Toast.LENGTH_LONG).show()
                }else{
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authentication...")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
                        .addOnCompleteListener() {
                            progressDialog.hide()
                            if (!it.isSuccessful) return@addOnCompleteListener
                            Toast.makeText(this,
                                "Successfully Login", Toast.LENGTH_SHORT).show()
                            val intent = Intent (this, SignInActivity::class.java)
                            startActivity(intent)
                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(this,
                                "Username/password incorrect", Toast.LENGTH_SHORT).show()
                        }
                }
            }
            R.id.txSignUp ->{
                var intent = Intent(this, SignUpActivity::class.java)
                startActivity(intent)

            }
        }
    }
}